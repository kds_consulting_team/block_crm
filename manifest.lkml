constant: connection {
  value: "keboola_block_crm_hubspot"
}

# url of your Salesforce domain for object links
constant: hubspot_account_id {
  value: "505715"
}
